(ns mastermind.jeu
	(:require [mastermind.fonctions :as fon])
	(:require [clojure.string :as str]))

(defn jeu "Jeu PC(encodeur) vs Joueur(craquer)" []
	(println "\nPC va generer le code de longueure " @fon/lenCode)
	(println "Le code (resp. tentative) doit etre tapper comme suite de couleurs separe par des espaces (ex: blanc bleu noir noir)")
    (println "Couleurs possibles sont: bleu, jaune, rouge, vert, noir, blanc.")
	(println "Vous aves" @fon/nbTentMax " tentatives pour deviner le code\n")
	(let [code (fon/code-secret @fon/lenCode)]  ;;generation du code
		(loop [numTent 1]          ;;boucle de tentatives
			(if (>= @fon/nbTentMax numTent)
				(do 
					(println "Tentative numero " numTent)
					(let [tent (fon/lecture-code @fon/lenCode)		;;lecture de tentative donné au clavier par joueur
						  indications (fon/filtre-indications code tent (fon/indications code tent))] ;;calcule des indications filtrées
						(println "Les indications selon la position : "      ;;affichage des indications
							(str/replace (str/join " " indications) ":" "") "\n")
						(if (every? #(= % :good) indications)				;; test si le code est deviné
							(do 
								(println "Le code est deviné")
								(println "Vous avez gagnez. Felicitations!\n"))
							(recur (inc numTent)))))			;; sinon le joueur fait une autre tentative
				(do             ;; cas ou le joueur utilisé tous ses tentatives
					(println "Vous avez utilise tous vos tentatives")
					(println "Le code etais : "
						(str/replace (str/join " " code) ":" ""))
					(println "Vous avez perdu...\n"))))))		