(ns mastermind.core
  (:require [mastermind.fonctions :as fon])
  (:require [mastermind.jeu :as jeu])
  (:require [mastermind.solveur :as sol])
  (:require [clojure.string :as str])
  (:gen-class))

(defn changezLongCode "Action qui permet modifier le longueure du code" []
	(println "Donnez nouvelle longueure du code : ")
	(let [len 
			(try (Integer/parseInt (read-line)) 
				(catch NumberFormatException e @fon/lenCode))]
		(if (or (< len 1) (> len 20))
			(do
				(println "Le code est trop large ou trop petit !")
				(println "La plage des valeurs autorises son entre 1 et 20"))
			(reset! fon/lenCode len))))

(defn changezNbTentMax "Action qui permet modifier nombre des tentative maximal" []
	(println "Donnez nouvelle nombre des tentative maximal: ")
	(let [nb 
			(try (Integer/parseInt (read-line)) 
				(catch NumberFormatException e @fon/nbTentMax))]
		(if (or (< nb 1) (> nb 100))
			(do
				(println "Le nombre de tentative est trop grand ou trop petit !")
				(println "La plage des valeurs autorises son entre 1 et 100"))
			(reset! fon/nbTentMax nb))))

(defn changezCouleurs "Action qui permet modifier les couleurs disponible" []
	(println "Donnez nouvelles couleurs: ")
	(let [code (map keyword (str/split (str/trim (read-line)) #"\s+"))]
		(loop [v code res []]
			(if (seq v)
				(if (some #(= % (first v)) res)
					(recur (rest v) res)
					(recur (rest v) (conj res (first v))))
				(reset! fon/couleurs res)))))

(defn menu-jeu "Menu principale du jeu" []
	(loop [sortie false]
		(if-not sortie
			(let [choix (do 
							(println "\n" "Choisissez le mode du jeu ou une action (donnez une chiffre)")
							(println "(1) PC genere le code et vous le craquez")
							(println "(2) Vous generez le code et PC va appliquer le solver pour le craquer")
							(println "(3) Changer le longueure du code (actuelle longueure = " @fon/lenCode ")")
							(println "(4) Changer le nombre de tentative maximal (actuelle nombre = " @fon/nbTentMax ")")
							(println "(5) Changer les couleurs disponible (actuelles couleurs = " (str/replace (str/join " " @fon/couleurs) ":" "") ")")
							(println "(6) Exit")
							(str/trim (read-line)))]
				(cond
					(= choix "1") (do (jeu/jeu) (recur false))
					(= choix "2") (do (sol/solveur) (recur false))
					(= choix "3") (do (changezLongCode) (recur false))
					(= choix "4") (do (changezNbTentMax) (recur false))
					(= choix "5") (do (changezCouleurs) (recur false))
					(= choix "6") (recur true)
					:else
					(do (println "Choix invalide") (recur false)))))))

(defn -main
  "Fonction entree"
  [& args]
  (println "Bienvenue dans le jeu Master Mind !")
  (menu-jeu)
  (println "Au revour !"))