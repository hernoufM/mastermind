(ns mastermind.fonctions
  (:require [clojure.string :as str]))

(def couleurs (atom [:noir :blanc :bleu :vert :jaune :rouge]))
(def nbTentMax (atom 12))
(def lenCode (atom 4))

(defn lecture-code "Permet lire le code ou tentative au clavier" [n]
  (loop [valide false res []]
    (if-not valide
      (do 
        (println "Donnez votre code(resp. tentative) du longuere" n " :")
        (let [code (str/split (str/trim (read-line)) #"\s+")]
          (cond
            (not= n (count code)) (do (println "Nombre de pions fautive! Veuillez reesseyer.") (recur false res))
            (not-every? (fn [c] (some #(= % c) @couleurs)) (map keyword code)) (do (println "Il y a des couleurs invalides! Veuillez reesseyer.") (recur false res))
            :else
            (recur true (map keyword code)))))
      res)))


(defn code-secret [n]
  (loop [i n, res []]
    (if (= i 0)
      res
      (recur (dec i) (conj res (clojure.core/rand-nth @couleurs))))))

(defn indications [v1 v2]
  (loop [code v1
         tent v2
         res []]
    (if (seq code)
      (recur (rest code) (rest tent) (cond
                                       (= (first code) (first tent)) (conj res :good)
                                       (some (partial = (first tent)) v1) (conj res :color)
                                       :else
                                       (conj res :bad)))
      res)))

(defn frequences [v]
  (loop [s v, res {}]
    (if (seq s)
      (recur (rest s) (assoc res (first s) (if (contains? res (first s))
                                             (inc (get res (first s)))
                                             1)))
      res)))


(defn freqs-dispo [v1 v2]
  (loop [code v1
         indic v2
         res (frequences v1)]
    (if (seq code)
       (if (= :good (first indic))
        (recur (rest code) (rest indic) (assoc res (first code) (dec (get res (first code)))))
        (recur (rest code) (rest indic) res))
       res)))

(defn filtre-indications [v1 v2 v3]
      (loop [freqv (frequences v1)
             tent v2
             indic v3
             res []]
        (let [ft (first tent)
              fi (first indic)]
          (if (seq tent)
            (if (get freqv ft)
              (if (= (get freqv ft) 0)
                (recur freqv (rest tent) (rest indic) (conj res :bad))
                (recur (assoc freqv ft (dec (get freqv ft))) (rest tent) (rest indic) (conj res fi)))
              (recur freqv (rest tent) (rest indic) (conj res fi)))
            res))))
