(ns mastermind.solveur
	(:require [mastermind.fonctions :as fon])
	(:require [clojure.string :as str]))

(defn initialiserPossibilities "Permet d initialiser map des possibilities" [n]
	(loop [i 0 res {}]
		(if (< i n)
			(recur (inc i) (assoc res i []))
			res)))

(defn premiereTentative "Permet de generer premiere tentative et effectuer mise a jour de couleursPourVerifier" 
	[couleursPourVerifier]
	(loop [i @fon/lenCode cvp couleursPourVerifier tent [] termine false]
		(if (> i 0)
			(if (seq cvp)
				(recur (dec i) (rest cvp) (conj tent (first cvp)) termine)
				(recur i couleursPourVerifier tent true))
			(if termine
				[[] tent]
				[cvp tent]))))

(defn mise-a-jour-possibilities-color "mise a jour du vector des possibilitie en cas de :color"
	[couleur indice possibilities couleursPresents positionsReglees]
	(if (contains? couleursPresents couleur)
		(if (some #(= % couleur) (vals positionsReglees))
			(loop [i 0 res possibilities]
				(if (< i @fon/lenCode)
					(if (and (not= i indice) (every? #(not= % i) (keys positionsReglees)))
						(if (some #(= % couleur) (get res i))
							(recur (inc i) res)
							(recur (inc i) (assoc res i (conj (get res i) couleur))))
						(recur (inc i) res))
					res))
			(let [vect (get possibilities indice)]
				(loop [v vect res []]
					(if (seq v)
						(if (= (first v) couleur)
							(recur (rest v) res)
							(recur (rest v) (conj res (first v))))
						(assoc possibilities indice res)))))
		(loop [i 0 res possibilities]
				(if (< i @fon/lenCode)
					(if (and (not= i indice) (every? #(not= % i) (keys positionsReglees)))
						(if (some #(= % couleur) (get res i))
							(recur (inc i) res)
							(recur (inc i) (assoc res i (conj (get res i) couleur))))
						(recur (inc i) res))
					res))))

(defn mise-a-jour-possibilities-good "mise a jour du vector des possibilitie en cas de :color"
	[couleur indice possibilities positionsReglees]
		(loop [i 0 res possibilities]
			(if (< i @fon/lenCode)
				(if (= indice i)
					(recur (inc i) (assoc res i [couleur]))
					(if (some #(= % i) (keys positionsReglees))
						(recur (inc i) res)
						(if (some #(= % couleur) (get res i))
							(recur (inc i) res)
							(recur (inc i) (assoc res i (conj (get res i) couleur))))))
				res)))

(defn mise-a-jour-total "mise a jour de tous les structure des donnes utilise par mon algorithme" 
[tentative indications possibilities couleursPresents positionsReglees]
	(loop [ind indications 
		   i 0
		   possibilities possibilities
		   couleursPresents couleursPresents
		   positionsReglees positionsReglees]
		(if (seq ind)
			(cond 
				(= (first ind) :color) (let [cp (conj couleursPresents (nth tentative i)) 
					  					  	 possib (mise-a-jour-possibilities-color (nth tentative i) i 
											 			possibilities couleursPresents positionsReglees)]
									  		(recur (rest ind) (inc i) possib cp positionsReglees))
				(= (first ind) :good)  (let [cp (conj couleursPresents (nth tentative i))
											possib (mise-a-jour-possibilities-good (nth tentative i) i 
											 			possibilities positionsReglees)
											posreg (assoc positionsReglees i (nth tentative i))]
														(recur (rest ind) (inc i) possib cp posreg))	
			:else
				(recur (rest ind) (inc i) possibilities couleursPresents positionsReglees))
		[possibilities couleursPresents positionsReglees])))

(defn calculerTentative "Calcule le tentative suivante prenant en compte les possibilities"
	[couleursPourVerifier possibilities positionsReglees]
	(loop [cpv couleursPourVerifier
		   possibilities possibilities
		   couleursInterdites #{}
		   tentative []
		   possNew possibilities
		   termine (not (seq couleursPourVerifier))]
		(let [[i vect] (first possibilities)]
			(cond
				(not (seq possibilities)) [(if termine [] cpv) possNew tentative]
				(some #(= % i) (keys positionsReglees)) 
					(recur cpv (rest possibilities) couleursInterdites 
						(conj tentative (get positionsReglees i)) possNew termine)
				(not (seq cpv)) 
					(if (seq vect) 
						(recur cpv (rest possibilities) couleursInterdites 
							(conj tentative (first vect))
								(assoc possNew i (conj (subvec vect 1) (first vect))) true)
						(recur couleursPourVerifier possibilities couleursInterdites tentative possNew true))
				(not (seq vect)) 
					(recur (rest cpv) (rest possibilities) couleursInterdites 
						(conj tentative (first cpv)) possNew termine)
				:else
				(let [coul 
					  (loop [v vect]
					  	(if (seq v)
					  		(if (contains? couleursInterdites (first v))
					  			(recur (rest v))
					  			(first v))
					  		nil))]
					  (if coul
					  	(if termine 
					  		(recur cpv (rest possibilities) (conj couleursInterdites coul) 
					  			(conj tentative coul) (assoc possNew i (conj (subvec vect 1) (first vect))) termine)
					  		(recur (rest cpv) (rest possibilities) couleursInterdites 
								(conj tentative (first cpv)) possNew termine))
					  	(recur (rest cpv) (rest possibilities) couleursInterdites 
					  		(conj tentative (first cpv)) possNew termine)))))))

(defn solveur "Solver Joueur(encodeur) vs PC(craquer)" []
	(println "\nVous devez tapper le code de longueure " @fon/lenCode ".")
	(println "Le code (resp. tentative) doit etre tapper comme suite de couleurs separe par des espaces (ex: blanc bleu noir noir).")
    (println "Couleurs possibles sont: bleu, jaune, rouge, vert, noir, blanc.")
    (println "Solveur va calculer et afficher a la fin nombre de tentatives utilise pour dechiffrer le code.")
    (println "Solveur utilise strategie (extra efficace!!!) de resolution elaboré par auteur de ce projet (Hernouf Mohamed).\n")
    (let [code (fon/lecture-code @fon/lenCode)]
    	(loop [numTent 0			;;boucle de tentatives
    		   couleursPourVerifier @fon/couleurs
    		   possibilities (initialiserPossibilities @fon/lenCode)
    		   tentative []
    		   indications []
    		   couleursPresents #{}
    		   positionsReglees {}]
			(println "\nTentative numero " numTent)
			(if (= numTent 0) 								;;premier tentative
				(let [[cpv tent] (premiereTentative couleursPourVerifier)]
					(do 
						(println "Ma premiere tentative = " (str/replace (str/join " " tent) ":" "") "\n") 
						(recur (inc numTent) cpv possibilities tent 
							(fon/filtre-indications code tent (fon/indications code tent)) 
							couleursPresents positionsReglees)))
				(if (not-every? #(= % :good) indications) 
					(let [[possib cp posreg] 
							(mise-a-jour-total tentative indications possibilities couleursPresents positionsReglees)
					  	  [cpv poss tent] 
					  	  	(calculerTentative couleursPourVerifier possib posreg)]
						(do
							(println "Les indications : " (str/replace (str/join " " indications) ":" ""))
							(println "Ma tentative selon les indications : " (str/replace (str/join " " tent) ":" ""))
							(recur (inc numTent) cpv poss tent 
								(fon/filtre-indications code tent (fon/indications code tent)) cp posreg)))
					(do 
						(println "Le code est :" (str/replace (str/join " " tentative) ":" ""))
						(println "\n\nJ'ai fait " numTent " tentative\n")))))))
